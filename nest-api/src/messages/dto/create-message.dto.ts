import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional, IsString } from "class-validator";
export class CreateMessageDto {

    @IsString()
    @IsNotEmpty()
    @ApiProperty({description:"the body of the message", minLength:1})
    text: string;
    
    @IsString({ each: true})
    @IsNotEmpty({each:true})
    @ApiProperty({description:"an array containing at least one recipients IDs",  minLength:1})
    recipientsIDs: string[];

    @IsOptional()
    @IsString({ each: true})
    @ApiProperty({description:"not yet implemented", minLength:0, required:false})
    seenBy: string[];

}
